# New Project
## Projet Jeu JS

L'objectif de ce projet est de faire un petit jeu en Javascript sans framework.
J'ai utilisé le JS, l'algorithmie de base, la manipulation du DOM et la POO dans un contexte ludique...
J'ai choisi memory comme jeux.

## Organisation

maquettes fonctionnelles du jeu
Pour chaque fonction/méthode, je fais la JSDoc


## Aspects techniques :

Utilisation des objets Javascript.
Avoir des données dans le JS qui seront modifiées en cours de jeu ( score, etc.).

## Réalisation
* Création de l'interface en dure en HTML avec les classes CSS correspondantes 
* Créations de deux Class js une PlayRound et Carte
* Reproduction de l'interface dans le DOM 
* la Class principale ou celle que j'ai utilisé le plus c'est PlayRound 
* Dans cette classe la première propriété est un tableau de Carte
* Des propriétés et des méthodes que je j'ai essayé de les bien commenter.

# Maquette


![wireframe Projet-JeuJS](public/img/phase1.png)
![wireframe Projet-JeuJS](public/img/phase2.png)
![wireframe Projet-JeuJS](public/img/phase3.png)

**lien**
https://henda85.gitlab.io/projet-jeu

