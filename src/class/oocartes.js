import { Carte } from './oocarte';

export class PlayRound {
    tab;
    /**
    * @type {Carte}
    */
    firstCarte = '';
    /**
    * @type {Carte}
    */
    secondCarte = '';
    /**
    * @type {Number}
    */
    counter = 0;//va contenir 0, 1,OU 2 
    /**
    *
    * @type {number}
    */
    vscore = 0;//score
    /**
    *@param {tab[]} tab
    */
    attempt = 0;//nombre de tentetive
    constructor(tab = []) {
    this.tab = tab;
    }
    /**
    * fonction qui va nous afficher l'ensemble des cartes
    *
    */
    toHTML() {
        let div = document.createElement('div');
        for (let carte of this.tab) {
            let divcarte = document.createElement('div');
            divcarte.classList.add('carte');
            divcarte.appendChild(carte.toHTML());
            div.appendChild(divcarte);
        }
        return div;
        }
    /**
    *fonction qui prend en paramétre une carte selectionner
    par l'utilisateur et va affecter cette carte à first carte 
    ou à second carte
    *
    */
    chooseCarte(carte) {
        
        if (carte === this.firstCarte) {
            return;
        }
        this.counter++;
        if (this.counter < 3) {
            if (this.counter === 1) {
                this.firstCarte = carte;
                this.firstCarte.classList.add('selected');
                //console.log(this.firstCarte);
                //console.log(this.counter);
                }
            else {
                this.secondCarte = carte;
                this.secondCarte.classList.add('selected');
                //console.log(this.secondCarte);
                //console.log(this.counter);
            }
        }}
        /**
        *fonction qui va comparer les deux cartes
        *choisies par l'utilisateur ...
        */
        isEqualCarte(){
        if (this.firstCarte !== '' && this.secondCarte !== '') {
            if (this.firstCarte.src !== this.secondCarte.src) {
            console.log("c'est pas bon");
            this.attempt++;
            this.caché();
            //this.secondCarte.src='img/caché.jpg';
            //this.firstCarte.src='img/caché.jpg';
            //retourne les deux cartes
            } else {
            console.log("c'est bon");
            this.firstCarte.classList.add('disparet');
            this.secondCarte.classList.add('disparet');
            this.vscore++;
            this.attempt++;
            //suprime les deux cartes
            //incrimente tentative
            //incrimente le score
        }
        this.reinit();
    }
    }

        /**
        *fonction qui va intiliser:
        *le compteur et les deux cartes choisie
        *
        */
        reinit() {
        this.firstCarte.classList.remove('selected');
        this.secondCarte.classList.remove('selected');
        this.counter = 0;
        this.firstCarte = '';
        this.secondCarte = '';
        }
        caché() {
            this.firstCarte.src = 'img/caché.jpg';   
            this.secondCarte.src = 'img/caché.jpg';
        }

        /**
        * fonction qui va melanger les cartes
        *
        */
        randomize(){
        let i;
        let j;
        let tmp;
            for(i = this.tab.length - 1; i > 0; i--){
                j = Math.floor(Math.random() * (i + 1));
                tmp = this.tab[i];
                this.tab[i] = this.tab[j];
                this.tab[j] = tmp;
            }
        }
    }
