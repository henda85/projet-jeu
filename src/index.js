import { Carte } from './class/oocarte';
import { PlayRound } from './class/oocartes';

let cartes = new PlayRound();
cartes.tab = [
  new Carte('chien1', 'img/chien1.jpg'),
  new Carte('chien2', 'img/chien2.jpg'),
  new Carte('chien3', 'img/chien3.jpg'),
  new Carte('chien4', 'img/chien4.jpg'),
  new Carte('chien5', 'img/chien5.jpg'),
  new Carte('chien6', 'img/chien6.jpg'),
  new Carte('chien1', 'img/chien1.jpg'),
  new Carte('chien2', 'img/chien2.jpg'),
  new Carte('chien3', 'img/chien3.jpg'),
  new Carte('chien4', 'img/chien4.jpg'),
  new Carte('chien5', 'img/chien5.jpg'),
  new Carte('chien6', 'img/chien6.jpg'),
];
let divCadre = document.querySelector('#cadre');
let vscore = document.querySelector('.vscore');
let attempt = document.querySelector('.attempt');  
let previousTarget = null;
cartes.randomize();

divCadre.appendChild(cartes.toHTML());
divCadre.addEventListener('click', (e) => {
  if ((e.target.className = 'carte')) {
    let cliked = e.target;
    if (
      cliked.nodeName === 'SECTION' ||
      cliked.nodeName === 'DIV' ||
      previousTarget === cliked) {
      return;
    }
    //cartes.chooseCarte(cliked);
    cartes.chooseCarte(cliked);
    cartes.isEqualCarte();
    vscore.textContent  = 'score : ' + cartes.vscore;
    attempt.textContent = 'nombre de tentative : ' + cartes.attempt;
    previousTarget = cliked;
  }
})

;
